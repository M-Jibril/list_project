  // jshint esversion:6
      window.onload = function() {
        var form = document.getElementById("form");
        var input = document.getElementById("input");
        var button = document.getElementById("button");
        var list = document.getElementById("list");
        var header = document.getElementById("header");
        var id = 1;

        button.addEventListener("click", addToList);

        list.addEventListener("click", boxChecked);

        list.addEventListener("click", removeFromList);

        header.addEventListener("click", hideTask);

        function addToList() {
          if (input.value === "") {
            alert("Please enter a task into the input field.");
          } else {
            let text = input.value;
            let item = `<li id= "li-${id}"><div class="d-inline">${text}</div>
                        <button class="btn btn-danger btn-sm d-inline" type="button" id="button-${id}"><i class="fas fa-times"></i></button>
                        <input class="form-check-input position-static d-inline" type="checkbox" id="checkbox-${id}" value="completed">
                      </li>`;

            list.insertAdjacentHTML("beforeend", item);
            id++;
            form.reset();
          }
        }

        function boxChecked(event) {
          let element = event.target;
          if (element.type === "checkbox" && element.checked === true) {
            element.parentNode.style.textDecoration = "line-through";
          } else if (element.checked === false) {
            element.parentNode.style.textDecoration = "none";
          }
        }

        function removeFromList() {
          let element = event.target;
          if (element.type === "button") {
            element.parentNode.remove();
          }
        }

        function hideTask() {
          let element = event.target;
          let hideBox = document.getElementsByClassName("position-static");
          for (let i = 0; i < hideBox.length; i++) {
            if (element.type === "checkbox" && element.checked === true) {
              if (hideBox[i].checked === true) {
                hideBox[i].parentNode.style.display = "none";
              }
            } else {
              hideBox[i].parentNode.style.display = "";
            }
          }
        }
      };

var myApp = angular.module('myApp', []);

myApp.controller('myCtrl', ['$scope', function ($scope) {
    $scope.taskList = [];

    $scope.addTask = function () {
        if ($scope.input == null) {
            alert("Please enter a task into the input field.");
        } else {
            $scope.taskList.push({text: $scope.input, completed: false});
            $scope.input = null;
        }
    };

    $scope.removeTask = function (index) {
        $scope.taskList.splice(index, 1)
    };

 }]);


myApp.directive('listItem', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: true,
        template: '<li id= "li-{{$index}}" ng-hide="task.completed && hide">\n' +
            '       <div class="d-inline" ng-class="{strike: task.completed}">{{$index + 1}}. {{task.text}}</div>\n' +
            '       <button class="btn btn-danger btn-sm d-inline" type="button" id="button-{{$index}}" ng-click="removeTask($index)">\n' +
            '       <i class="fas fa-times"></i>\n' +
            '       </button>\n' +
            '       <input class="form-check-input position-static d-inline" type="checkbox" id="checkbox-{{$index}}" ng-model="task.completed">\n' +
            '      </li>',

    }
});
